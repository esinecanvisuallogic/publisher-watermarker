package org.esinecan.publisher.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.util.StringUtils.isEmpty;

/**
 * A base class for exception handlers in service projects. Since our services consume each other, or relay through
 * Zuul, this project is our client-library in a sense. Therefore ExceptionHandling approach should be kept here.
 *
 * Created by eren.sinecan
 */
public abstract class BaseExceptionHandler {
    private static final ExceptionMapping DEFAULT_ERROR = new ExceptionMapping(
            "SERVER_ERROR",
            "Internal server error",
            INTERNAL_SERVER_ERROR);

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BaseExceptionHandler.class);
    private final Map<Class, ExceptionMapping> exceptionMappings = new HashMap<>();

    /**
     * Default constructor that registers some of the common Exceptions.
     */
    public BaseExceptionHandler() {
        registerMapping(
                MissingServletRequestParameterException.class,
                "MISSING_PARAMETER",
                "Missing request parameter",
                BAD_REQUEST);
        registerMapping(
                MethodArgumentTypeMismatchException.class,
                "ARGUMENT_TYPE_MISMATCH",
                "Argument type mismatch",
                BAD_REQUEST);
        registerMapping(
                HttpRequestMethodNotSupportedException.class,
                "METHOD_NOT_SUPPORTED",
                "HTTP method not supported",
                METHOD_NOT_ALLOWED);
        registerMapping(
                ServletRequestBindingException.class,
                "MISSING_HEADER",
                "Missing header in request",
                BAD_REQUEST);
    }

    /**
     * This method checks if the Exception or its cause has been registered. If so, logs and wraps it into an
     * ErrorResponse entitiy. Otherwise returns the entity with a default 500 - "Internal Server Error"
     *
     * @param ex The exception that is currently being handled
     * @param response The response we're intercepting
     * @return A dto with the error code and message we assigned to ex.class
     */
    @ExceptionHandler(Throwable.class)
    public ErrorResponse handleThrowable(final Throwable ex, final HttpServletResponse response) {
        ExceptionMapping mapping = exceptionMappings.getOrDefault(ex.getClass(),
                exceptionMappings.getOrDefault(ex.getCause().getClass(), DEFAULT_ERROR));

        response.setStatus(mapping.status.value());

        String message = isEmpty(ex.getMessage()) ? mapping.message : ex.getMessage();

        logger.error("{} ({}): {}", message, mapping.code, ex.getMessage(), ex);

        return new ErrorResponse(mapping.message, mapping.code);
    }

    protected void registerMapping(
            final Class<?> clazz,
            final String code,
            final String message,
            final HttpStatus status) {
        exceptionMappings.put(clazz, new ExceptionMapping(code, message, status));
    }

    @Data
    public static class ErrorResponse {
        private final String code;
        private final String message;
    }

    @AllArgsConstructor
    private static class ExceptionMapping {
        private final String message;
        private final String code;
        private final HttpStatus status;
    }
}