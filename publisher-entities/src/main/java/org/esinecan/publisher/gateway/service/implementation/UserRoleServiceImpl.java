package org.esinecan.publisher.gateway.service.implementation;

import org.esinecan.publisher.gateway.model.Role;
import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.gateway.repository.UserRepository;
import org.esinecan.publisher.gateway.service.contract.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by eren.sinecan
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRepository userRepository;

    /**`
     * Retrieves up to date roles of users.
     *
     * @param username Simply the username acquired from Principal at Controller level
     * @return Roles of the user as comma separated values
     */
    @Override
    @Transactional(readOnly = true)
    public String getUserRolesCSV(String username) {
        User user = userRepository.findByUsername(username);
        Collection<Role> roles = user.getRoles();
        return roles.stream().map(role -> role.getName()).collect(Collectors.joining(","));
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> getUserLandingUrls(String username) {
        User user = userRepository.findByUsername(username);
        Collection<Role> roles = user.getRoles();
        return roles.stream().map(role -> role.getFirstLandingUrl()).collect(Collectors.toList());
    }
}
