package org.esinecan.publisher.gateway.service.contract;

import java.util.List;

/**
 * Created by eren.sinecan
 */
public interface UserRoleService {
    String getUserRolesCSV(String username);
    List<String> getUserLandingUrls(String username);
}
