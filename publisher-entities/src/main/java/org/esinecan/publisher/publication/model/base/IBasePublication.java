package org.esinecan.publisher.publication.model.base;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.esinecan.publisher.publication.model.Book;
import org.esinecan.publisher.publication.model.Journal;

import static org.esinecan.publisher.publication.PublicationStrategyConstants.BOOK_CODE;
import static org.esinecan.publisher.publication.PublicationStrategyConstants.JOURNAL_CODE;

/**
 * Created by eren.sinecan
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "name")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Book.class, name = BOOK_CODE),
        @JsonSubTypes.Type(value = Journal.class, name = JOURNAL_CODE),
})
public interface IBasePublication {
}
