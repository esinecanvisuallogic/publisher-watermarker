package org.esinecan.publisher.publication.repository;

import org.esinecan.publisher.publication.model.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by eren.sinecan
 */
@Repository
public interface TopicRepository extends JpaRepository<Topic, Long> {
    List<Topic> findByLabel(String label);
}
