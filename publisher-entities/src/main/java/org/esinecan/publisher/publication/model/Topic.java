package org.esinecan.publisher.publication.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * The entity that holds 'Topic's that 'Book's belong to. Currently we have three: Science, Business, Media
 *
 * Created by eren.sinecan
 */
@Entity
@Table(name = "topics")
@Getter
@Setter
@NoArgsConstructor
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String label;
}
