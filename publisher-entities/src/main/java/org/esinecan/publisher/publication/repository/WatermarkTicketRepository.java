package org.esinecan.publisher.publication.repository;

import org.esinecan.publisher.publication.model.WatermarkTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by eren.sinecan
 */
@Repository
public interface WatermarkTicketRepository extends JpaRepository<WatermarkTicket, Long> {
}
