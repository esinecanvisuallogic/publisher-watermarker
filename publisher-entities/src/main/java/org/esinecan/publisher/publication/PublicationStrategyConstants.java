package org.esinecan.publisher.publication;

/**
 * String constants that denote the "nickname" of each type of publication.
 *
 * These are utilized by Jackson to help with the inherited type mapping and as a discriminator at the enum called
 * 'PublicationStrategy'
 *
 * Created by eren.sinecan
 */
public class PublicationStrategyConstants {
    public static final String BOOK_CODE = "Book";
    public static final String JOURNAL_CODE = "Journal";
}
