package org.esinecan.publisher.publication.model;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.esinecan.publisher.publication.model.base.BasePublication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import static org.esinecan.publisher.publication.PublicationStrategyConstants.JOURNAL_CODE;

/**
 * This is the entity of 'Book' type of publication. It does not have any attributes its super class does not have, but
 * it still does have its own table.
 *
 * Created by eren.sinecan
 */
@Entity
@Table(name = "journals")
@DiscriminatorValue(JOURNAL_CODE)
@Getter
@Setter
@NoArgsConstructor
@JsonTypeName(JOURNAL_CODE)
public class Journal extends BasePublication {
}
