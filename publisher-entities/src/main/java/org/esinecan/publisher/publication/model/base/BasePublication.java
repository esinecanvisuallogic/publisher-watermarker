package org.esinecan.publisher.publication.model.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.publication.model.WatermarkTicket;

import javax.persistence.*;

/**
 * This is the entity class that holds common attributes which will be used by every existing and future type of
 * publication.
 *
 * Created by eren.sinecan
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
/**
 * We don't need a discriminator as the inheritance is not Single but we add it for normalization.
 */
@DiscriminatorColumn(name="PUB_TYPE", discriminatorType=DiscriminatorType.STRING)
@Getter
@Setter
@NoArgsConstructor
public abstract class BasePublication implements IBasePublication {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String content;

    @Column
    private String title;

    @Column
    private String author;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "watermarkTicket")
    private WatermarkTicket watermarkTicket;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user")
    @JsonIgnore
    private User user;
}
