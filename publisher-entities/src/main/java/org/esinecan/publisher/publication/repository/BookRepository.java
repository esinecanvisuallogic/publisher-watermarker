package org.esinecan.publisher.publication.repository;

import org.esinecan.publisher.publication.model.Book;
import org.esinecan.publisher.publication.model.WatermarkTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by eren.sinecan
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    Book findByWatermarkTicket(WatermarkTicket watermarkTicket);
}
