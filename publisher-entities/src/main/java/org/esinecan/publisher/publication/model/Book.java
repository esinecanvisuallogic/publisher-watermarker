package org.esinecan.publisher.publication.model;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.esinecan.publisher.publication.model.base.BasePublication;

import javax.persistence.*;

import static org.esinecan.publisher.publication.PublicationStrategyConstants.BOOK_CODE;

/**
 * This is the entity of 'Book' type of publication. It has the 'Topic' field which is also an entity for extensibility
 * purposes.
 *
 * Created by eren.sinecan
 */
@Entity
@Table(name = "books")
@DiscriminatorValue(BOOK_CODE)
@Getter
@Setter
@NoArgsConstructor
@JsonTypeName(BOOK_CODE)
public class Book extends BasePublication {

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "topic")
    private Topic topic;
}
