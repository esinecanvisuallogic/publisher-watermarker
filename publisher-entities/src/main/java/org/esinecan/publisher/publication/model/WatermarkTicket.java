package org.esinecan.publisher.publication.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.joda.time.Instant;

import javax.persistence.*;

/**
 * This is the 'Watermark' entity that has a one to one relationship with the publications.
 *
 * I chose to make the relationship unidirectional as we would not need that extra join column and there is no need to
 * violate Principle of Least Privilege.
 *
 * Created by eren.sinecan
 */
@Entity
@Table(name = "watermarks")
@Getter
@Setter
@NoArgsConstructor
public class WatermarkTicket {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Boolean completed;

    @Column
    private Boolean started;

    @Column
    private Boolean failed;

    @Column
    private Instant startInstant;
}
