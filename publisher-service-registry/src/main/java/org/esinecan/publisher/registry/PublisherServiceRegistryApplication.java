package org.esinecan.publisher.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * This is our Eureka Server. Our microservices will register themselves here. Then they will be able to send each other
 * FeignHystrix requests by microservice name instead of the host information.
 *
 * Created by eren.sinecan
 */
@SpringBootApplication
@EnableEurekaServer
public class PublisherServiceRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(PublisherServiceRegistryApplication.class, args);
	}
}
