package org.esinecan.publisher.watermarker.config;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Security configuration to require the "USER" role to access write endpoints. Same as application service. Our flow
 * does not really have any client side access to this service, and instead of making a security configuration we could
 * have just limited the IP addresses that can send a request to this server to localhost, but we're planning ahead for
 * when we have multiple instances on many servers.
 *
 * Created by eren.sinecan
 */
@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class WatermarkerSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Basic auth is enough. Since we don't get any XHR requests, we don't need CSRF
        http.httpBasic().and().csrf().disable();
    }
}
