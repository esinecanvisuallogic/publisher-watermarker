package org.esinecan.publisher.watermarker.web;

import org.esinecan.publisher.publication.model.WatermarkTicket;
import org.esinecan.publisher.watermarker.service.WatermarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The endpoint that receives the incoming Feign call from the application microservice.
 *
 * Created by eren.sinecan
 */
@RestController
public class WatermarkController {

    @Autowired
    private WatermarkService watermarkService;

    @RequestMapping(value = "/watermark-ticket", method = RequestMethod.POST)
    public Boolean watermarkTicket(@RequestBody WatermarkTicket ticket) throws InterruptedException {
       return watermarkService.watermarkPublication(ticket);
    }
}
