package org.esinecan.publisher.watermarker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by eren.sinecan
 */
@SpringBootApplication(scanBasePackages = {"org.esinecan.publisher"})
@EnableEurekaClient
@ComponentScan({"org.esinecan.publisher"})
@EntityScan("org.esinecan.publisher")
@EnableJpaRepositories("org.esinecan.publisher")
@EnableFeignClients
public class PublisherWatermarkApplication {
    public static void main(String[] args) {
        SpringApplication.run(PublisherWatermarkApplication.class, args);
    }
}
