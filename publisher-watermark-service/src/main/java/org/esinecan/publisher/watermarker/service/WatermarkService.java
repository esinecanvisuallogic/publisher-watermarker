package org.esinecan.publisher.watermarker.service;

import io.reactivex.Single;
import org.esinecan.publisher.publication.model.WatermarkTicket;
import org.esinecan.publisher.publication.repository.WatermarkTicketRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Mock service that waits for a duration (configured at config-repo, watermarkingDurationInMiliseconds property. has a
 * fallback value of 180000 miliseconds) so that we're able to illustrate asynchronousness of our application, and we
 * have the time to query the ticket before it's completed.
 *
 * Then it marks the ticket as complete.
 * Created by eren.sinecan
 */
@Service
public class WatermarkService {

    private static final Logger logger = LoggerFactory.getLogger(WatermarkService.class);

    @Autowired
    private WatermarkTicketRepository watermarkTicketRepository;

    @Value("${watermarkingDurationInMiliseconds:180000}")
    private Integer watermarkingDurationInMiliseconds;

    public Boolean watermarkPublication(WatermarkTicket ticket){

        logger.debug("Ticket has been passed to publisher-watermark-service: {}", ticket.getId());

        if(!ticket.getCompleted()){
            Single s = Single.create(singleEmitter -> {
                Thread thread = new Thread(() -> {
                    try {
                        Thread.sleep(watermarkingDurationInMiliseconds);
                        ticket.setCompleted(true);
                        ticket.setStarted(false);
                        watermarkTicketRepository.saveAndFlush(ticket);
                        logger.debug("publisher-watermark-service Totally watermarked for ticket: {}", ticket.getId());
                    }catch (Exception e){
                        logger.error("publisher-watermark-service could not watermark ticket: {} " +
                                "for the following reason: {}", ticket.getId(), e);
                        singleEmitter.onError(e);
                    }
                });
                thread.start();
            }).doOnError(throwable -> {
                logger.error("publisher-watermark-service miserably failed for ticket: {}", ticket.getId());
            });

            s.subscribe().dispose();

            logger.debug("Reactive thread has been started for ticket: {}, it is up to god now.", ticket.getId());

            return true;
        }
            return false;
    }
}
