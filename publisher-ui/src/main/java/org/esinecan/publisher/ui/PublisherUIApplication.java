package org.esinecan.publisher.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is the UI app that just generates and serves the static assets. It's actual address is obfuscated to th end user
 * as it's relayed from 'publisher-gateway' through Zuul.
 *
 * Created by eren.sinecan
 */
@SpringBootApplication
public class PublisherUIApplication {

	public static void main(String[] args) {
		SpringApplication.run(PublisherUIApplication.class, args);
	}
}
