package org.esinecan.publisher.ui.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by eren.sinecan
 */
@RestController
public class URLConfiguration {

    @Value("${publisher.base.url:http://localhost:9010}")
    private String baseUrl;

    @RequestMapping("/base")
    public String getBaseUrl(Principal principal) {
        return "\"" + baseUrl + "\"";
    }

}
