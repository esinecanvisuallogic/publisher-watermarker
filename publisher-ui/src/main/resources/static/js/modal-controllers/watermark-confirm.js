angular.module('clientapp').controller('watermarkConfirm', function ($uibModalInstance, publication, $http, $rootScope){
    var $ctrl = this;

    $ctrl.publication = publication;

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $ctrl.ok = function () {
        $http.post($rootScope.baseUrl + 'app/watermark-publication', $ctrl.publication.watermarkTicket)
            .then(function(response) {
                $uibModalInstance.close(response);
            });
    };
});