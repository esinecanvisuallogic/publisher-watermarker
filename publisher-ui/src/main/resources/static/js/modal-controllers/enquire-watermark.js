angular.module('clientapp').controller('enquireWatermark', function ($uibModalInstance, $http, $rootScope){
    var $ctrl = this;

    $ctrl.close = function () {
        $uibModalInstance.dismiss('cancel');
    };



    $ctrl.enquire = function () {
        $http.post($rootScope.baseUrl + 'app/is-watermark-complete', $ctrl.ticketId)
            .then(function(response) {
                $ctrl.ticket = response.data;
            });
    };
});