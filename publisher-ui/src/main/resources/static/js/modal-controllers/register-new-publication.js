angular.module('clientapp').controller('registerNewPublication', function ($uibModalInstance, publicationTypes, $http, $rootScope){

    var $ctrl = this;

    $ctrl.publicationTypes = publicationTypes;

    $ctrl.publicationDTO = {};

    $ctrl.getTopics = function () {
        $http.get($rootScope.baseUrl + 'app/get-topics').then(function(response) {
            $ctrl.topics = response.data;
        }, function(errResponse) {
            $ctrl.topics = [];
        });
    };
    $ctrl.getTopics();

    $ctrl.changeCategory = function (category) {
        $ctrl.publicationDTO.name = category;
        if(category.name != "Book"){
            $ctrl.publicationDTO.topic = undefined;
        }
    };

    $ctrl.changeTopic = function (topic) {
        $ctrl.publicationDTO.topic = topic;
    }

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $ctrl.ok = function () {
        $http.put($rootScope.baseUrl + 'app/register-new-publication', $ctrl.publicationDTO)
            .then(function(response) {
                $uibModalInstance.close(response);
            });
    };
});