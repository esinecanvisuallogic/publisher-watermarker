angular.module('clientapp', ['ui.bootstrap']).config(function($httpProvider) {

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

}).controller('home',

    function($http, $rootScope, $uibModal, $log, $timeout, $document, $interval) {

        var self = this;

        /** BEGIN: Initial values */

        //The three boolean values that we use to decide which publications to pull from back end
        self.getPubsRqBody = {};
        self.getPubsRqBody.excludeWatermarked = false; //Initially we want to show everything
        self.getPubsRqBody.excludeWatermarkInProgress = false;
        self.getPubsRqBody.excludeNonWatermarked = false;

        //No success or error messages initially
        self.showSuccessMsg = false;
        self.successMsg = "";
        self.showErrorMsg = false;
        self.errorMsg = "";

        //We did not confirm user's authentication yet. (Though if they made it here, they most likely are.)
        self.authenticated = false;
        self.user = "nobody";

        //We need these three here as well to be able to discriminate publications easier. Somewhat front end version
        // of what we do on back end
        self.BOOK_CODE = "Book";
        self.JOURNAL_CODE = "Journal";
        self.publicationTypes = [self.BOOK_CODE, self.JOURNAL_CODE];

        //No publications yet
        self.pubs = [];

        //we set this value to true whenever we're about to send a get my publications request and set it back to false
        //when the result comes.
        self.refreshing = false;

        /** END: Initial values */

        /**
         * The function that fills the grid based on booleans in self.getPubsRqBody
         */
        self.fillData = function () {
            self.refreshing = true;
            $http.post($rootScope.baseUrl + 'app/get-my-publications', self.getPubsRqBody).then(function(response) {
                self.refreshing = false;
                if (response.data){
                    self.pubs = response.data;
                }
            }, function(errorResponse){
                self.displayError(errorResponse.data.message);
            });
        };

        /**
         * Initialization function. Confirms user authentication, fills the grid excluding nothing
         */
        self.initialization = function(){
            $http.get('base').then(function(response) {
                $rootScope.baseUrl = response.data;
                $http.get($rootScope.baseUrl + 'app/user').then(function(response) {
                    var data = response.data;
                    if (data.name) {
                        self.authenticated = true;
                        self.user = data.name;
                        self.modalParent =
                            angular.element($document[0].querySelector('.content-holder'));
                        self.fillData();

                        //Actually let's update the grid every 10 seconds if the user is authenticated.
                        $interval(self.fillData(), 10000);
                    } else {
                        self.authenticated = false;
                    }
                }, function() {
                    self.authenticated = false;
                });
            });
        };

        /**
         * sends a request to gateway's logout url
         */
        self.logout = function() {
            $http.post($rootScope.baseUrl + 'logout', {}).finally(function() {
                self.authenticated = false;
                window.location = $rootScope.baseUrl;
            });
        };

        /**
         * Opens up a pop up that can be utilized to register a new publication
         */
        self.registerNewPublication = function () {
            var modalInstance = $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/ui/modal-templates/register-new-publication.html',
                controller: 'registerNewPublication',
                controllerAs: '$ctrl',
                appendTo: self.modalParent,
                resolve: {
                    publicationTypes: function () {
                        return self.publicationTypes;
                    }
                }
            });

            modalInstance.result.then(function (response) {
                if(response.status == 200){
                    self.displaySuccess("Your publication was created successfully.");
                }else{
                    self.displayError(response.message);
                }
                self.fillData();
            }, function () {
                $log.info('canceled by user action.');
                self.fillData();
            });
        };

        /**
         * Opens up a pop up that takes confirmation from the user to call the watermarking endpoint.
         * If the user confirms, calls the endpoint and re-populates the grid excluding nothing.
         */
        self.waterMarkPublication = function (publication) {
            var modalInstance = $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/ui/modal-templates/watermark-confirm.html',
                controller: 'watermarkConfirm',
                controllerAs: '$ctrl',
                appendTo: self.modalParent,
                resolve: {
                    publication: function () {
                        return publication;
                    }
                }
            });

            modalInstance.result.then(function (response) {
                if(response.status == 200){
                    self.displaySuccess("Your publication is being watermarked.");
                }else{
                    self.displayError(response.message);
                }
                self.fillData();
            }, function () {
                $log.info('canceled by user action.');
                self.fillData();
            });
        };

        /**
         * Opens up a modal window that has a close button. Takes in ticketId, displays status.
         */
        self.enquireWatermarkCompletenessByTicketNumber = function () {
            var modalInstance = $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/ui/modal-templates/enquire-watermark.html',
                controller: 'enquireWatermark',
                controllerAs: '$ctrl',
                appendTo: self.modalParent
            });

            modalInstance.result.then(function (response) {
                self.fillData();
            }, function () {
                $log.info('canceled by user action.');
                self.fillData();
            });
        };

        /**
         * Produces some random publications for testing
         */
        self.generateRandom = function () {
            $http.post($rootScope.baseUrl + 'app/generate-random', {}).finally(function() {
                self.fillData();
            });
        }

        /**
         * This function is called when we receive an error. It displays the error on top for 3 seconds.
         *
         * @param errMsg The error message from the server side controller.
         */
        self.displayError = function (errMsg) {

            self.showErrorMsg = true;
            self.errMsg = errMsg;

            $timeout(function () {
                self.showErrorMsg = false;
                self.errorMsg = "";
            }, 3000);

        };

        /**
         * This function is called when we do a successful transaction. It displays the message on top for 3 seconds.
         *
         * @param succMsg The success message from the client side controller. (self)
         */
        self.displaySuccess = function (succMsg) {

            self.showSuccessMsg = true;
            self.successMsg = succMsg;

            $timeout(function () {
                self.showSuccessMsg = false;
                self.successMsg = "";
            }, 3000);

        };

        //Now that we've defined our things, let's start.
        self.initialization();
    });