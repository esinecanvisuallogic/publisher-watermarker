package org.esinecan.monitoring.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by eren.sinecan
 */
@SpringBootApplication
@EnableEurekaClient
@EnableHystrixDashboard
@RestController
public class PublisherServiceMonitoringApplication {
    public static void main(String[] args) {
        SpringApplication.run(PublisherServiceMonitoringApplication.class, args);
    }

    @RequestMapping("/isalive")
    public String alivecheck(){
        return "yup";
    }
}
