# Publisher-Watermarker - A Spring Cloud Exercise #

## Scenario ##
This is a mock app to be used by publishers. Supposedly, you're an agent working at a publisher. You want your 
 publications to be watermarked so that they are not used without the permission of your publishing house. On your first
 day of work:

- You open this application, register, and login.
- You register a new publication using such data as author, title, content, etc.
- Once the publication is registered, you're able to watermark it. You try to watermark.
- The mock-watermark service waits for 3 minutes on a separate thread, then marks the publication as watermarked.
- You're able to query the status and confirm this. 

## Project Structure ##
(For more info, you can find the Javadoc in the project root directory)

### publisher-service-registry ###
This is a Spring Cloud Netflix Eureka instance to facilitate client-side service discovery. Since our publisher-application-service and publisher-watermark-service need to communicate via Feign calls, we have set this up to allow these two to register themselves using this configuration:

```
#!yaml

eureka:
  client:
    serviceUrl:
      defaultZone: ${EUREKA_URI:http://localhost:8761/eureka}
  instance:
    preferIpAddress: true
```
Actually all of our services inherit this configuration as we have put this into our root application.yml file in config-repo. But the only ones that actually benefit from this are the application-service and watermarker-service.

### publisher-config-service ###
This is a Spring Cloud Config Server. There is some shared configuration between the services: 
    
- They all use the same H2 embedded database.
- They all connect to the same embedded Redis instance to be able to share the user session. 
- All of them have the logging level of DEBUG. 
- They all use the USER role with READ and WRITE privileges.
- If they are a business microservice, they will register themselves to the same Eureka Server instance. (Preferred over Consul for ease of testing)

So they will be inheriting these common properties from the default profile's (Which is the only profile we have. As
 is mock application only to be run on localhost, I did not feel the need to add multiple profiles) application.yml but 
  they have their own yml config files for their unique properties such as the port number the application will run on.
  
This application uses a repository called config-repo to read these values, which I will mention in the [How to Run](#How-to-Run) section.

### publisher-gateway ###
This is a Spring Cloud project implemented following API Gateway Pattern. So until you register and login, you're only
 interacting with publisher-gateway and after that it will relay your requests to the rest of the application. 
 It has it's own views, Controllers and Services to interact with the user until registration
 and login are done with. Looks like this: (Don't mind the "Created By PaintX" labels at bottoms. I'm using a very lame app to take screenshots)

Register Screen:
![watermarker-register2.jpg](https://bitbucket.org/repo/5qqxzXg/images/1272078942-watermarker-register2.jpg)

Login Screen:
![watermarker-login.jpg](https://bitbucket.org/repo/5qqxzXg/images/1226999-watermarker-login.jpg)

Once the user is in, it will start acting as a Zuul proxy and serve endpoints of publisher-application-service
 as well as static files if publisher-ui. This way, we're able to separate the application from authorization, and place
 the authorization as the outer layer, securing our application.
 
Our gateway still authenticates every XHR request seny by the user. But from here on out, it just routes those request to their appropriate service through Zuul. It uses the following routing located in the publisher-gateway.yml file
 inside config-repo:
 
    zuul:
      routes:
        app:
          path: /app/**
          url: http://localhost:9000
          sensitiveHeaders:
        ui:
          path: /ui/**
          url: http://localhost:9020
          sensitiveHeaders:
          
### publisher-watermark-service ###
This is our mock watermark service. It has an endpoint that takes feign requests sent by publisher-application-service. Then it starts another thread that just waits for a number of milliseconds that is configured in config-repo and served by publisher-config-service. We put the waiting time to illustrate asynchronous nature of the service calls and due to the fact that processing a document page by page would probably take some time.

After the specified amount of time has passed, it updates the watermark status of the document as complete. If, for some reason, it can't succeed in doing that, then it marks the ticket is failed and logs the error. Failed tickets can be retried whenever the user wants.

### publisher-application-service ###
This is the service that deals with the user XHR requests coming from the UI. It has endpoints that deal with registering a new publication, registering a bunch of new publications for easier testing, querying the status of watermarking of a publication, and starting the watermarking of a publication.

When we start the watermarking process, a Feign call is made by a HystrixCommand to the publisher-watermark-service which will mark it as complete later on. If it fails, fallBack method of the HystrixCommand updates the entity to specify it's failed.

### publisher-ui ###
This service only serves our static UI which is a very small AngularJS application to the authenticated users. It then sends XHR requests to our application service, and updates the UI accordingly. Generally the main screen looks like this:

![main.jpg](https://bitbucket.org/repo/5qqxzXg/images/3849049978-main.jpg)

Using the checkboxes above, the user can filter the ticket statuses displayed. There is also a refresh button. Also, using the buttons at the top, 

- if the user clicks "register a publication" a form opens in a modal window where the user can add a new publication.
![watermarker-newpub.jpg](https://bitbucket.org/repo/5qqxzXg/images/2853726637-watermarker-newpub.jpg)

- if the user clicks enquire by ticket number, a modal window opens where the user can enter the ticket number and display status
![watermark-enquire.jpg](https://bitbucket.org/repo/5qqxzXg/images/220491335-watermark-enquire.jpg)

- if the user clicks "Generate Random Data" a number of publications between 10-20 will be automatically created and the front end will refresh the grid.


## How to Run ##

- First we need to make a small bit of configuration for config server. Copy the config-repo directory from project root,
and paste it into your home directory. On Unix based systems, just navigate into the project folder from terminal, and 
run the command *cp -R config-repo/. ~/config-repo* and it should be ok. Then run the following:
    - cd ~/config-repo
    - git init
    - git add .
    - git commit -a -m "config repo initialization"
      
    For the last command, what you type into between quotation marks does not really matter provided it's not empty. 
    
- Then you need to run the projects. The order is half important here. I mean, some are interchangeable but for the sake of simplicity, here is my recommended order:

    1. *publisher-config-service*: Everyone needs their configurations
    1. *publisher-service-registry*: Services will want to register themselves
    1. *publisher-gateway*: This one starts the embedded Redis instance where we keep the sessions, others will depend on it. On a standalone Redis instance it won't be a problem
    1. *publisher-watermark-service*
    1. *publisher-ui*
    1. *publisher-application-service*: This one holds the initial DB config values such as our Topics. So it should be started the last 

This step must have seemed pretty straightforward for an IDEA
or Eclipse user. But if you're not using an IDE, following steps will also work:
    - navigate to project root from your terminal. Once you are there and you have an internet connection, start running these commands:

```
#!sh
        mvn clean install
        cd publisher-config-service
        mvn spring-boot:run &
        cd ../publisher-service-registry
        mvn spring-boot:run &
        cd ../publisher-gateway
        mvn spring-boot:run &
        cd ../publisher-watermark-service
        mvn spring-boot:run &
        cd ../publisher-ui
        mvn spring-boot:run &
        cd ../publisher-application-service
        mvn spring-boot:run &

```     
Then you can just visit [http://localhost:9010/](http://localhost:9010/)