package org.esinecan.publisher.gateway.unit;

import org.esinecan.publisher.gateway.unit.base.BaseServiceTest;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan
 */
public class UserRoleServiceTest extends BaseServiceTest {

    @Test
    public void testGetUserRolesCSV(){
        String role = userRoleService.getUserRolesCSV(username);
        assertEquals(role, "ROLE_USER");
    }

    @Test
    public void testGetUser(){
        List<String> urls = userRoleService.getUserLandingUrls(username);
        assertEquals(urls.size(), 1);
        assertEquals(urls.get(0), ("/ui/"));
    }
}
