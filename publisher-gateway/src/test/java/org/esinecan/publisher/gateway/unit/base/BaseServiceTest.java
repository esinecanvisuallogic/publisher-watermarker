package org.esinecan.publisher.gateway.unit.base;

import org.esinecan.publisher.gateway.PublisherAuthApplication;
import org.esinecan.publisher.gateway.auth.service.contract.UserService;
import org.esinecan.publisher.gateway.auth.service.implementation.UserDetailsServiceImpl;
import org.esinecan.publisher.gateway.auth.validation.error.EmailExistsException;
import org.esinecan.publisher.gateway.auth.web.dto.UserDto;
import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.gateway.repository.UserRepository;
import org.esinecan.publisher.gateway.service.contract.UserRoleService;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by eren.sinecan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {PublisherAuthApplication.class})
public abstract class BaseServiceTest {

    @Autowired
    public UserDetailsServiceImpl userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    public UserService userService;

    @Autowired
    public UserRoleService userRoleService;

    public User user;
    public String username = "testdetails@test.com";
    public String password = "password";

    @Before
    public void registerFirst(){
        User user = userRepository.findByUsername(username);
        if (user == null) {
            UserDto userDto = new UserDto();
            userDto.setUsername(username);
            userDto.setPassword(password);
            userDto.setMatchingPassword(password);

            try {
                this.user = userService.registerNewUserAccount(userDto);
            } catch (EmailExistsException e) {
                e.printStackTrace();
            }
        }
    }
}
