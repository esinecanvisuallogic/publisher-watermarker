package org.esinecan.publisher.gateway.features;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.esinecan.publisher.gateway.PublisherAuthApplication;
import org.esinecan.publisher.gateway.auth.service.contract.UserService;
import org.esinecan.publisher.gateway.auth.web.dto.UserDto;
import org.esinecan.publisher.gateway.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by eren.sinecan
 */
@ContextConfiguration(
        loader = SpringBootContextLoader.class,
        classes = PublisherAuthApplication.class
)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CreateNewUserSteps {

    private final String username = "test@test.com";

    private final String password = "password";

    private UserDto userDto = new UserDto();

    private User user;

    @Autowired
    private UserService userService;

    @Given("^a username, 'test@test\\.com', a password 'password' is present$")
    public void a_username_test_test_com_a_password_password_is_present() throws Throwable {
        userDto.setUsername(username);
        userDto.setPassword(password);
        userDto.setMatchingPassword(password);
    }

    @When("^user data is submitted$")
    public void user_data_is_submitted() throws Throwable {
        user = userService.registerNewUserAccount(userDto);
    }

    @Then("^I should see a new user$")
    public void i_should_see_a_new_user() throws Throwable {
        assertTrue(user instanceof User);

        assertEquals(user.getUsername(), username);
    }
}
