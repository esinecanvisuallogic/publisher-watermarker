Feature: User registration

Narrative:
First step of using
application from a user perspective,
would be creating a user first.

Scenario: Registering a user

Given a username, 'test@test.com', a password 'password' is present
When user data is submitted
Then I should see a new user