package org.esinecan.publisher.gateway.auth.web.controller;

import org.esinecan.publisher.gateway.auth.service.contract.UserService;
import org.esinecan.publisher.gateway.auth.validation.error.EmailExistsException;
import org.esinecan.publisher.gateway.auth.web.dto.UserDto;
import org.esinecan.publisher.gateway.service.contract.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Standard issue auth endpoints.
 *
 * Created by eren.sinecan
 */
@RestController
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService userRoleService;
    /**
     * This endpoint is to figure out if the user is already authenticated and setting the auth headers. Thus, it
     * includes the up-to-date role info too.
     *
     * @param user Currently logged in user
     * @return an Authentication in the form of a map.
     */
    @RequestMapping("/user")
    public Map<String, Object> user(Principal user) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("name", user.getName());
        map.put("roles", map.put("roles", userRoleService.getUserRolesCSV(user.getName())));
        return map;
    }

    /**
     * Reads user's roles and returns potential landing pages
     *
     * @param user Currently logged in user
     * @return landing pages Url list.
     */
    @RequestMapping("/login")
    public List<String> login(Principal user) {
        return userRoleService.getUserLandingUrls(user.getName());
    }


    /**
     * New user endpoint.
     *
     * @param accountDto A DTO with the data of the user to be registered.
     * @return a boolean indicator of success or failure.
     * @throws EmailExistsException thrown when the user was already registered.
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public boolean registerUserAccount(@Valid @RequestBody UserDto accountDto) throws EmailExistsException {
        userService.registerNewUserAccount(accountDto);
        return true;
    }

}
