package org.esinecan.publisher.gateway.auth.service.implementation;

import org.esinecan.publisher.gateway.model.Role;
import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.gateway.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Our custom implementation of UserDetailsService of Spring Security.
 *
 * Created by eren.sinecan
 */
@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService{

    @Autowired
    private UserRepository userRepository;

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    /**
     * This method is the adapter between our auth schema (consisting of Role, Authority and User) and Spring's own.
     *
     * @param username user's e-mail address
     * @return User account metadata
     * @throws UsernameNotFoundException thrown when there is no such user.
     */
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        UserDetails userDetails = new org.springframework.security.core.userdetails.User
                (user.getUsername(),
                user.getPassword(),
                enabled,
                accountNonExpired,
                credentialsNonExpired,
                accountNonLocked,
                getAuthorities(user.getRoles()));

        logger.debug("User details object retrieved: {}", userDetails);
        return userDetails;
    }

    private final Collection<? extends GrantedAuthority> getAuthorities(final Collection<Role> roles) {
        return getGrantedAuthorities(roles.stream().map(role -> role.getName()).collect(Collectors.toList()));
    }

    private final Set<GrantedAuthority> getGrantedAuthorities(final List<String> roles) {
        return roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
    }
}
