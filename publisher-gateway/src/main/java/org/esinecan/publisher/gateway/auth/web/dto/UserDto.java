package org.esinecan.publisher.gateway.auth.web.dto;

import lombok.Getter;
import lombok.Setter;
import org.esinecan.publisher.gateway.auth.validation.annotation.PasswordMatches;
import org.esinecan.publisher.gateway.auth.validation.annotation.ValidEmail;
import org.esinecan.publisher.gateway.auth.validation.annotation.ValidPassword;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * User object that arrives the controller upon login/registration
 *
 * Created by eren.sinecan
 */
@PasswordMatches
@Getter
@Setter
public class UserDto {

    @NotNull
    @NotEmpty
    @ValidPassword
    private String password;

    @NotNull
    @NotEmpty
    private String matchingPassword;

    @NotNull
    @NotEmpty
    @ValidEmail
    private String username;
}
