package org.esinecan.publisher.gateway.auth.validation.validator;

import org.esinecan.publisher.gateway.auth.validation.annotation.PasswordMatches;
import org.esinecan.publisher.gateway.auth.web.dto.UserDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator to check if password and confirm password fields match.
 *
 * Created by eren.sinecan
 */
public class PasswordMatchesValidator
        implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }
    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context){
        UserDto user = (UserDto) obj;
        return user.getPassword().equals(user.getMatchingPassword());
    }
}