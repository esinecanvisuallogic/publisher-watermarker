package org.esinecan.publisher.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class PublisherAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(PublisherAuthApplication.class, args);
	}
}
