package org.esinecan.publisher.gateway.auth.service.contract;

import org.esinecan.publisher.gateway.auth.validation.error.EmailExistsException;
import org.esinecan.publisher.gateway.auth.web.dto.UserDto;
import org.esinecan.publisher.gateway.model.User;

/**
 * Contract for UserServiceImpl
 *
 * Created by eren.sinecan
 */
public interface UserService {

    User registerNewUserAccount(UserDto accountDto) throws EmailExistsException;
}
