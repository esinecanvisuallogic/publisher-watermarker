package org.esinecan.publisher.gateway.auth.configuration;

import org.esinecan.publisher.gateway.configuration.RoleConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Since we use H2 as the database, data resets every time we restart the app. These are some configurations we need
 * when we start the app.
 *
 * Created by eren.sinecan
 */
@Component
public class InitialDataLoader implements
        ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private InitializationPropertiesConfiguration initializationPropertiesConfiguration;

    @Autowired
    private RoleConfigurer roleConfigurer;

    /**
     * Checks if the roles and privileges specified in <em>initialization.properties</em> exist.
     * Creates them otherwise.
     * @param event the event Spring broadcasts when the ApplicationContext instance is refreshed.
     */
    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        roleConfigurer.configureRolesAndPrivileges(initializationPropertiesConfiguration.getUserRole(),
                initializationPropertiesConfiguration.getReadPrivilege(),
                initializationPropertiesConfiguration.getWritePrivilege(),
                initializationPropertiesConfiguration.getAfterLoginUrl());
    }


}