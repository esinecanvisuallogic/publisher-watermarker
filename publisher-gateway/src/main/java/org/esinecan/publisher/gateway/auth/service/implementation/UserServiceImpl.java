package org.esinecan.publisher.gateway.auth.service.implementation;

import org.esinecan.publisher.gateway.auth.configuration.InitializationPropertiesConfiguration;
import org.esinecan.publisher.gateway.auth.service.contract.UserService;
import org.esinecan.publisher.gateway.auth.validation.error.EmailExistsException;
import org.esinecan.publisher.gateway.auth.web.dto.UserDto;
import org.esinecan.publisher.gateway.model.Role;
import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.gateway.repository.RoleRepository;
import org.esinecan.publisher.gateway.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by eren.sinecan
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private InitializationPropertiesConfiguration initializationPropertiesConfiguration;

    @Autowired
    private RoleRepository roleRepository;


    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    /**
     * Registers a user to our system, encrypts their password in DB.
     *
     * @param userDto user object to be registered
     * @return registered user
     * @throws EmailExistsException thrown when username is a duplicate
     */
    @Override
    @Transactional
    public User registerNewUserAccount(UserDto userDto) throws EmailExistsException {
        if (emailExists(userDto.getUsername())){
            throw new EmailExistsException("There is already an account with that username adress: " +userDto.getUsername());
        }
        else {
            Collection<Role> roles = Collections.singletonList(roleRepository.findByName(
                    "ROLE_" + initializationPropertiesConfiguration.getUserRole()
            ));

            User user = new User();
            user.setUsername(userDto.getUsername());
            user.setPassword(passwordEncoder.encode(userDto.getPassword()));
            user.setRoles(roles);
            userRepository.save(user);

            logger.info("A new user has been registered: {}", user);

            return user;
        }
    }

    private boolean emailExists(String username) {
        User user = userRepository.findByUsername(username);
        if (user != null) {
            return true;
        }
        return false;
    }
}
