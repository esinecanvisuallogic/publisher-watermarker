angular.module('gateway', []).config(function($httpProvider) {

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

}).controller('navigation',

    function($http, $window) {

        var self = this;

        /** BEGIN: Initial values */

        //The user will fill this variable on the form at html template.
        self.credentials = {};

        //This is the username of logged in user.
        self.user = '';

        //initially the user is not logged in
        self.authenticated = false;

        //This variable displays a mini error message component when set to true.
        self.error = false;

        //This is the content of said error.
        self.errMsg = "";

        //Conversely, this variable controls a small success announcement div's visibility.
        self.registerSuccess = false;

        /**
         * Sets the X-Requested-With headers and sends spring security a request to acquire a user session and a CSRF
         * token. If there is already a valid token in the cookies, user will be taken in straight away.
         *
         * @param credentials
         * @param callback
         */
        self.login = function() {

            var headers = self.credentials ? {
                authorization : "Basic "
                + btoa(self.credentials.username + ":"
                    + self.credentials.password)
            } : {};

            $http.get('user', {
                headers : headers
            }).then(function(response) {
                var data = response.data;
                if (data.name) {
                    self.authenticated = true;
                    self.user = data.name
                    self.registerError = false;
                    self.registerSuccess = false;
                    $http.get('login', {
                        headers : headers
                    }).then(function(landingUrlsResponse) {
                        var landingUrls = landingUrlsResponse.data;
                        if (landingUrls.length > 1) {
                            self.landingUrls = landingUrls
                        } else {
                            $window.location.href = landingUrls[0];
                        }
                    });
                } else {
                    self.authenticated = false;
                }
            }, function(){
                if(self.credentials.username){
                    self.error = true;
                    self.registerSuccess = false;
                    self.errMsg = "Wrong username or password";
                }
            });
        };

        /**
         * Depending on the checkbox value at the login/register page, signs the user in or registers a new user.
         */
        self.loginOrRegister = function () {
            self.registerSuccess = false;
            self.registerError = false;
            self.error = false;
            if(self.registerMode){
                self.register();
            }else{
                self.login();
            }
        };

        /**
         * Sends a post request to registration endpoint
         */
        self.register = function () {
            $http.post('register', self.credentials).then(function(response) {
                if (response.data){
                    self.registerMode = false;
                    self.registerSuccess = true;
                    self.error = false;
                }
            }, function(errorResponse){
                self.error = true;
                self.registerSuccess = false;
                self.errMsg = errorResponse.data.message;
            });
        };

        /**
         * Invalidates user session
         */
        self.logout = function() {
            $http.post('logout', {}).finally(function() {
                self.authenticated = false;
                self.registerSuccess = false;
                self.error = false;
            });
        };

        //We're all set. Let's see if the token in user's cookies valid:
        self.login();
    });