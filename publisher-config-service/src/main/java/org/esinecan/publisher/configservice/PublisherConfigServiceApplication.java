package org.esinecan.publisher.configservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * This project does not really have any implementation in it as it's sole responsibility is to become the global
 * configuration server so that we don't have to copy-paste yml/properties files between our microservice projects.
 *
 * Created by eren.sinecan
 */
@SpringBootApplication
@EnableConfigServer
public class PublisherConfigServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PublisherConfigServiceApplication.class, args);
	}
}
