package org.esinecan.publisher.app.service;

import org.esinecan.publisher.app.client.WatermarkCommand;
import org.esinecan.publisher.app.service.strategies.PublicationStrategy;
import org.esinecan.publisher.app.service.strategies.contract.IPublicationService;
import org.esinecan.publisher.app.service.strategies.implementation.BookService;
import org.esinecan.publisher.app.service.strategies.implementation.JournalService;
import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.publication.model.WatermarkTicket;
import org.esinecan.publisher.publication.model.base.IBasePublication;
import org.esinecan.publisher.publication.repository.WatermarkTicketRepository;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This is the entry point of service layer of our application. Controller methods call this class, and this class
 * traverses the strategies to find the right service for whatever method is called.
 *
 * Created by eren.sinecan
 */
@Service
public class PublicationStrategyWrapperService implements IPublicationService{

    @Autowired
    private BookService bookService;

    @Autowired
    private JournalService journalService;

    @Autowired
    private WatermarkTicketRepository watermarkTicketRepository;

    @Autowired
    private WatermarkCommand watermarkCommand;

    private List<IPublicationService> publicationServices = new ArrayList<>();

    private static final Logger logger = LoggerFactory.getLogger(PublicationStrategyWrapperService.class);

    /**
     * The method that registers our member service instances.
     */
    @PostConstruct
    private void addPublicationService(){
        publicationServices.add(bookService);
        publicationServices.add(journalService);
    }

    /**
     * Traverses through autowired publication service instances, finds and returns the appropriate one.
     *
     * @param strategy enum instance that registers the publication type
     * @return The autowired strategy implementation instance for the PublicationStrategy input.
     */
    private IPublicationService getService(PublicationStrategy strategy){
        Optional<IPublicationService> publicationService =
                publicationServices.stream()
                        .filter(iPublicationServicePredicate -> {
                            String s = iPublicationServicePredicate.getClass().getName();
                            if(s.contains("$$")){
                                return s.split("\\$\\$")[0].equals(strategy.getServiceClass().getName());
                            }
                            return s.equals(strategy.getServiceClass().getName());}).findFirst();

        if(!publicationService.isPresent()){
            throw new IllegalArgumentException("Can't find the service for: " + strategy.name());
        }

        logger.debug("Found service {} from strategy {}", publicationService, strategy);
        return publicationService.get();
    }

    /**
     *
     * @param user current user, who will own the newly created publication
     * @param iBasePublication Newly created publication's not yet persisted entity
     * @return Newly created publication's persisted entity
     */
    @Override
    public IBasePublication registerNewPublication(User user, IBasePublication iBasePublication) {
        PublicationStrategy strategy = PublicationStrategy.getPublicationStrategyByClass(iBasePublication.getClass());
        IPublicationService publicationService = getService(strategy);
        return publicationService.registerNewPublication(user, iBasePublication);
    }

    /**
     * Returns a list of publications owned by the user.
     *
     * @param user owner of the publications we want to get.
     * @param excludeWatermarked Flag that tells our service not to return already watermarked publications
     * @param excludeWatermarkInProgress Flag that tells our service not to return publications currently being processed
     * @param excludeNonWatermarked Flag that tells our service not to return publications no one tried to watermark yet
     *
     * @return List of my publications
     */
    @Override
    public List<IBasePublication> getMyPublications(User user, Boolean excludeWatermarked,
                                                    Boolean excludeWatermarkInProgress,
                                                    Boolean excludeNonWatermarked) {
        List<IBasePublication> publications = new ArrayList<>();
        publicationServices.forEach(iPublicationService ->
                publications.addAll(
                        iPublicationService.getMyPublications(user, excludeWatermarked,
                                excludeWatermarkInProgress, excludeNonWatermarked)));

        return publications;
    }

    /**
     * Updates the WatermarkTicket of publication to indicate watermarking has started. Then it calls dummy watermarking
     * service asynchronously through a HystrixFeign call that is wrapped in a RxJava observable.
     *
     * @param ticket is the 'WatermarkTicket' object that will be watermarked.
     * @return the updated ticket of the supplied publication.
     */
    @Transactional
    public WatermarkTicket watermarkPublication(WatermarkTicket ticket) {
        if(!ticket.getCompleted() && (!ticket.getStarted() || ticket.getFailed())){
            if(watermarkCommand.execute(ticket)){
                ticket.setStarted(true);
                ticket.setStartInstant(Instant.now());
                watermarkTicketRepository.saveAndFlush(ticket);
            }
        }
        return ticket;
    }

    /**
     * Watermark entities do not interfere with the rest of the business once
     * initialized. Therefore this method is not needed to be implemented or
     * Overriden elsewhere.
     * @param ticketId Id of the ticket to be returned
     * @return a 'WatermarkTicket' object that contains the status info.
     */
    @Transactional(readOnly = true)
    public WatermarkTicket isWatermarkComplete(Long ticketId) {
        return watermarkTicketRepository.findOne(ticketId);
    }
}
