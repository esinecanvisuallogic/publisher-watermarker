package org.esinecan.publisher.app.web.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by eren.sinecan
 */
@NoArgsConstructor
@Setter
@Getter
public class GetMyPublicationsRequestBody {
    private Boolean excludeWatermarked;
    private Boolean excludeWatermarkInProgress;
    private Boolean excludeNonWatermarked;
}
