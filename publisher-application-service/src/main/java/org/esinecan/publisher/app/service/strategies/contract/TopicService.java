package org.esinecan.publisher.app.service.strategies.contract;

import org.esinecan.publisher.publication.model.Topic;

import java.util.List;

/**
 * Created by eren.sinecan
 */
public interface TopicService {
    List<Topic> getAllTopics();
}
