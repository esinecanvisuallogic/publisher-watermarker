package org.esinecan.publisher.app;

import org.esinecan.publisher.app.configuration.FeignClientConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.ConditionalOnEnabledHealthIndicator;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.turbine.EnableTurbine;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"org.esinecan.publisher"})
@EnableEurekaClient
@ComponentScan({"org.esinecan.publisher"})
@EntityScan("org.esinecan.publisher")
@EnableJpaRepositories("org.esinecan.publisher")
@EnableFeignClients(defaultConfiguration = FeignClientConfiguration.class)
@EnableHystrix
@EnableTurbine
@EnableCircuitBreaker
public class PublisherApplicationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PublisherApplicationServiceApplication.class, args);
	}
}
