package org.esinecan.publisher.app.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

/**
 * Security configuration to require the "USER" role to access write endpoints.
 *
 * Created by eren.sinecan
 */
@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private AppInitializationPropertiesConfiguration appInitializationPropertiesConfiguration;

    /**
     * We
     * @param http XHR security builder of Spring
     * @throws Exception can be thrown by HttpBasicConfigurer or ExpressionUrlAuthorizationConfigurer
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // We need this to prevent the browser from popping up a dialog on a 401
        http.httpBasic().disable().csrf()
                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
        http.authorizeRequests()
                .antMatchers("/turbine.stream").permitAll()
                .antMatchers("/hystrix.stream").permitAll()
                .antMatchers(HttpMethod.POST, "/**")
                .hasRole(appInitializationPropertiesConfiguration.getUserRole())
                .anyRequest().authenticated();
    }
}
