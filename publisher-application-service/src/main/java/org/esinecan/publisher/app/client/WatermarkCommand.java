package org.esinecan.publisher.app.client;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.esinecan.publisher.publication.model.WatermarkTicket;
import org.esinecan.publisher.publication.repository.WatermarkTicketRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by eren.sinecan on 25/04/2017.
 */
@Service
public class WatermarkCommand {

    private final WatermarkClient watermarkClient;

    @Autowired
    private WatermarkTicketRepository watermarkTicketRepository;

    /**
     * Here, WatermarkTicketRepository is injected into the Application Context.
     * @param watermarkClient instance of Feign client WatermarkClient
     */
    @Autowired
    public WatermarkCommand(WatermarkClient watermarkClient){
        this.watermarkClient = watermarkClient;
    }

    private static final Logger logger = LoggerFactory.getLogger(WatermarkCommand.class);

    /**
     * This method only calls the WatermarkClient's Feign endpoint method. But due to the annotation above it, it wraps
     * Feign calls Hystrix calls so that we can utilize the fallBack functionality.
     * ew
     * @param ticket
     * @return
     */
    @HystrixCommand(fallbackMethod = "fallBack", commandKey = "CompletableWatermarkCommand" )
    public Boolean execute(WatermarkTicket ticket){
        return this.watermarkClient.watermarkTicket(ticket);
    }

    public Boolean fallBack(WatermarkTicket ticket) throws Exception {
        logger.error("fallback executed for ticket: {}", ticket);
        ticket.setFailed(true);
        ticket.setStarted(false);
        watermarkTicketRepository.saveAndFlush(ticket);
        throw (new Exception("Watermarking failed for ticket: " + ticket.getId()));
    }
}
