package org.esinecan.publisher.app.service.strategies.implementation;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import org.esinecan.publisher.app.service.strategies.contract.IPublicationService;
import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.publication.model.Book;
import org.esinecan.publisher.publication.model.QBook;
import org.esinecan.publisher.publication.model.QWatermarkTicket;
import org.esinecan.publisher.publication.model.WatermarkTicket;
import org.esinecan.publisher.publication.model.base.IBasePublication;
import org.esinecan.publisher.publication.repository.BookRepository;
import org.esinecan.publisher.publication.repository.WatermarkTicketRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A service class that deals with publications of type 'Book' transactional for safe asynchronous access purposes.
 *
 * Created by eren.sinecan
 */
@Service
@Transactional
public class BookService implements IPublicationService{

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private WatermarkTicketRepository watermarkTicketRepository;

    @Autowired
    private EntityManager entityManager;

    private static final Logger logger = LoggerFactory.getLogger(BookService.class);

    /**
     * Saves the publication with a ticket that indicates no watermarking has occurred yet.
     *
     * @param user the user that's currently logged in.
     * @param iBasePublication The publication dto to be saved. An instance of 'Book' class.
     * @return saved 'Book' object.
     */
    @Override
    public Book registerNewPublication(User user, IBasePublication iBasePublication) {
        WatermarkTicket ticket = new WatermarkTicket();
        ticket.setCompleted(false);
        ticket.setStarted(false);
        watermarkTicketRepository.saveAndFlush(ticket);

        Book book = (Book) iBasePublication;
        book.setUser(user);
        book.setWatermarkTicket(ticket);

        logger.debug("A new book was registered: {}", book);

        return bookRepository.save(book);
    }

    /**
     * We don't really need all of the functionality in this method, but I'm adding it for assessment purposes.
     * I used QueryDSL here because I'm a bit irked by the hard coded queries and their lack of type safety. On the
     * other hand, while JOOQ is a great tool, SqlResultSetMapping definitions seem eccentric to me. QueryDSL feels
     * like it blends the most within the syntax.
     *
     * @param user the user that's currently logged in.
     * @param excludeWatermarked do not return publications with 'complete' ticket statuses
     * @param excludeWatermarkInProgress do not return publications with 'started but incomplete' ticket statuses
     * @param excludeNonWatermarked do not return publications with 'has not started' ticket statuses
     * @return a list of publications owned by the user
     */
    @Override
    @Transactional(readOnly = true)
    public List<IBasePublication> getMyPublications(User user, Boolean excludeWatermarked,
                                                    Boolean excludeWatermarkInProgress,
                                                    Boolean excludeNonWatermarked) {
        QBook qbook = QBook.book;
        JPAQuery<?> query  = new JPAQuery<>(entityManager);

        QWatermarkTicket qTicket = QWatermarkTicket.watermarkTicket;
        JPAQuery<WatermarkTicket> subQuery = new JPAQuery<>(entityManager);

        //We will apply a mini builder pattern to this guy.
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(qbook.user.eq(user));

        if(excludeWatermarked){
            builder.and(qbook.watermarkTicket.completed.isFalse());
        }

        if(excludeWatermarkInProgress){
            builder.and(
                    qbook.watermarkTicket.completed.isFalse().and(
                            qbook.watermarkTicket.started.isFalse()
                    ));
        }

        if(excludeNonWatermarked){
            builder.and(qbook.watermarkTicket.started.isTrue());
        }

        logger.debug("Book publications were called with following params: " +
                "<user:{}, excludeWatermarked:{}, excludeWatermarkInProgress:{}, excludeNonWatermarked:{}>",
                user, excludeWatermarked, excludeWatermarkInProgress, excludeNonWatermarked);

        return query.select(qbook).from(qbook).where(builder.getValue()).fetch()
            .stream().map(IBasePublication.class::cast)
            .collect(Collectors.toList());
    }
}
