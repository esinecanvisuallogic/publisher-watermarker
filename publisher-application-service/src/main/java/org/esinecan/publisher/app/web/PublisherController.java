package org.esinecan.publisher.app.web;

import org.esinecan.publisher.app.service.PublicationStrategyWrapperService;
import org.esinecan.publisher.app.service.strategies.contract.AppUserService;
import org.esinecan.publisher.app.service.strategies.contract.TopicService;
import org.esinecan.publisher.app.service.util.GenerateRandomDataService;
import org.esinecan.publisher.app.web.dto.GetMyPublicationsRequestBody;
import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.publication.model.Topic;
import org.esinecan.publisher.publication.model.WatermarkTicket;
import org.esinecan.publisher.publication.model.base.IBasePublication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

/**
 * Created by eren.sinecan
 */
@RestController
public class PublisherController {

    @Autowired
    private TopicService topicService;

    @Autowired
    private AppUserService userService;

    @Autowired
    private PublicationStrategyWrapperService publicationStrategyWrapperService;

    @Autowired
    private GenerateRandomDataService generateRandomDataService;

    @RequestMapping(value = "/get-topics", method = RequestMethod.GET)
    public List<Topic> getTopics(){
        return topicService.getAllTopics();
    }

    @RequestMapping(value = "/register-new-publication", method = RequestMethod.PUT)
    public IBasePublication registerNewPublication
            (Principal principal, @RequestBody IBasePublication iBasePublication){
        User user = userService.getUserByUsername(principal.getName());
        return publicationStrategyWrapperService.registerNewPublication(user, iBasePublication);
    }

    /**
     * Not a POST operation but we want sensitive user data to be encrypted and uncacheable.
     *
     * @param principal Authentication info sent in request header. Used for deducing the active user.
     * @param body An object that has 3 booleans that decide on the selection criteria
     * @return a list of publications owned by the user
     */
    @RequestMapping(value = "/get-my-publications", method = RequestMethod.POST)
    public List<IBasePublication> getMyPublications
            (Principal principal, @RequestBody GetMyPublicationsRequestBody body){
        User user = userService.getUserByUsername(principal.getName());
        return publicationStrategyWrapperService.getMyPublications(user, body.getExcludeWatermarked(),
                body.getExcludeWatermarkInProgress(), body.getExcludeNonWatermarked());
    }

    @RequestMapping(value = "/watermark-publication", method = RequestMethod.POST)
    public WatermarkTicket watermarkPublication(@RequestBody WatermarkTicket ticket){
        return publicationStrategyWrapperService.watermarkPublication(ticket);
    }

    @RequestMapping(value = "/is-watermark-complete", method = RequestMethod.POST)
    public WatermarkTicket isWaterMarkComplete(@RequestBody Integer ticketId){
        return publicationStrategyWrapperService.isWatermarkComplete(Long.valueOf(ticketId));
    }

    @RequestMapping(value = "/generate-random", method = RequestMethod.POST)
    public List<IBasePublication> getRandomPublications(Principal principal){
        User user = userService.getUserByUsername(principal.getName());
        return generateRandomDataService.makeSomeStuffUpPls(user);
    }
}
