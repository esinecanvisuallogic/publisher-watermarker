package org.esinecan.publisher.app.service.strategies;

import org.esinecan.publisher.app.service.strategies.implementation.BookService;
import org.esinecan.publisher.app.service.strategies.implementation.JournalService;
import org.esinecan.publisher.publication.model.Book;
import org.esinecan.publisher.publication.model.Journal;
import org.esinecan.publisher.publication.model.base.BasePublication;

import java.util.Arrays;

import static org.esinecan.publisher.publication.PublicationStrategyConstants.BOOK_CODE;
import static org.esinecan.publisher.publication.PublicationStrategyConstants.JOURNAL_CODE;

/**
 * This is one of the central pieces of our application. We rely on Strategy pattern. This is where the decoupled
 * strategies converge.
 *
 * If our publisher decided on introducing a new kind of publication (i.e. 'OnlineCourse'), they would need to:
 *
 * <em>+</em> edit this class to add the new type, and tell our application the entity, service, and Jackson
 * differentiator String of the new service.
 * <em>+</em> edit PublicationStrategyConstants to introduce a new Jackson differentiator String constant.
 * <em>+</em> implement the entity within 'org.esinecan.publisher.publication.model' package and the service within
 * 'org.esinecan.publisher.app.service.strategies.implementation' package.
 * <em>+</em> register the service in the 'PublicationStrategyWrapperService' class.
 *
 * Created by eren.sinecan
 */
public enum PublicationStrategy {
    BOOK(BOOK_CODE, Book.class, BookService.class),
    JOURNAL(JOURNAL_CODE, Journal.class, JournalService.class);

    private String value;
    private Class<? extends BasePublication> publicationDTOClass;
    private Class<?> serviceClass;

    private PublicationStrategy(String value, Class<? extends BasePublication> publicationDTOClass,
                                Class<?> serviceClass){
        this.value = value;
        this.publicationDTOClass = publicationDTOClass;
        this.serviceClass = serviceClass;
    }

    public String getValue(){
        return value;
    }

    public Class<?> getServiceClass(){
        return serviceClass;
    }

    public Class<? extends BasePublication> getPublicationClass(){
        return publicationDTOClass;
    }

    public static PublicationStrategy getPublicationStrategyByValue(String value){
        return Arrays.stream(PublicationStrategy.values())
                .filter(e -> e.getValue().equals(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Unsupported type %s.", value)));
    }

    public static PublicationStrategy getPublicationStrategyByClass(Class<?> aClass){
        return Arrays.stream(PublicationStrategy.values())
                .filter(e -> {
                    PublicationStrategy strategy = PublicationStrategy.getPublicationStrategyByValue(e.getValue());
                    return strategy.getPublicationClass().equals(aClass);
                })
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Unsupported type %s.", aClass)));
    }
}
