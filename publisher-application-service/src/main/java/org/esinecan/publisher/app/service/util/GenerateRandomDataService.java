package org.esinecan.publisher.app.service.util;

import org.esinecan.publisher.app.service.PublicationStrategyWrapperService;
import org.esinecan.publisher.app.service.strategies.PublicationStrategy;
import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.publication.model.Book;
import org.esinecan.publisher.publication.model.Journal;
import org.esinecan.publisher.publication.model.Topic;
import org.esinecan.publisher.publication.model.base.IBasePublication;
import org.esinecan.publisher.publication.repository.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.esinecan.publisher.app.service.strategies.PublicationStrategy.BOOK;

/**
 * A helper class to generate some random test data for ease of testing
 *
 * Created by eren.sinecan
 */
@Service
@Transactional
public class GenerateRandomDataService {

    @Autowired
    private PublicationStrategyWrapperService publicationStrategyWrapperService;

    @Autowired
    private TopicRepository topicRepository;

    /**
     * Generates between 10-20 random publications
     *
     * @param user current User
     * @return List of IBasePublications
     */
    public List<IBasePublication> makeSomeStuffUpPls(User user){
        List<Topic> topics = topicRepository.findAll();

        return IntStream.range(1, ThreadLocalRandom.current().nextInt(10, 21)).
                parallel().mapToObj(value -> {
            PublicationStrategy strategy =
                    PublicationStrategy.values()[ThreadLocalRandom.current().
                            nextInt(0, PublicationStrategy.values().length)];

            IBasePublication publication;

            Topic randomTopic = topics.get(ThreadLocalRandom.current().
                    nextInt(0, topics.size()));

            if(strategy == BOOK){
                Book book = new Book();
                book.setTopic(randomTopic);
                book.setTitle("Title " + value);
                book.setAuthor("Author " + value);
                book.setContent("Content " + value);
                publication = book;
            }else{
                Journal journal = new Journal();
                journal.setTitle("Title " + value);
                journal.setAuthor("Author " + value);
                journal.setContent("Content " + value);
                publication = journal;
            }

            return publicationStrategyWrapperService.registerNewPublication(user, publication);
        }).collect(Collectors.toList());
    }
}
