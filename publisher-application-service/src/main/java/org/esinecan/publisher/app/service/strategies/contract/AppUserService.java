package org.esinecan.publisher.app.service.strategies.contract;

import org.esinecan.publisher.gateway.model.User;

/**
 * Created by eren.sinecan
 */
public interface AppUserService {

    User getUserByUsername(String username);
}
