package org.esinecan.publisher.app.service.strategies.contract;

import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.publication.model.base.IBasePublication;

import java.util.List;

/**
 * Created by eren.sinecan
 */
public interface IPublicationService {
    IBasePublication registerNewPublication(User user, IBasePublication iBasePublication);

    List<IBasePublication> getMyPublications(User user, Boolean excludeWatermarked,
                                             Boolean excludeWatermarkInProgress,
                                             Boolean excludeNonWatermarked);
}
