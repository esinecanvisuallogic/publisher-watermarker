package org.esinecan.publisher.app.configuration;

import org.esinecan.publisher.gateway.configuration.RoleConfigurer;
import org.esinecan.publisher.publication.model.Topic;
import org.esinecan.publisher.publication.repository.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Creates the roles and topics specified in specs on application boot time.
 * Created by eren.sinecan
 */
@Component
public class AppInitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private AppInitializationPropertiesConfiguration appInitializationPropertiesConfiguration;

    @Autowired
    private RoleConfigurer roleConfigurer;

    @Autowired
    private TopicRepository topicRepository;

    /**
     * We need to set up our topics (for books) when the application starts up. Since we use an in memory db, they'll
     * be gone every time we start the app. Therefore if we wished to expand the app through addition of some new
     * topics, (i.e. 'politics') this would be the place to do it. We also create two books for ease of testing.
     *
     * @param contextRefreshedEvent the event Spring broadcasts when the ApplicationContext instance is refreshed.
     */
    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        roleConfigurer.configureRolesAndPrivileges(appInitializationPropertiesConfiguration.getUserRole(),
                appInitializationPropertiesConfiguration.getReadPrivilege(),
                appInitializationPropertiesConfiguration.getWritePrivilege(), appInitializationPropertiesConfiguration.getAfterLoginUrl());

        List<Topic> topicList = topicRepository.findByLabel("Science");
        if(topicList == null || topicList.size() == 0){
            Topic topic = new Topic();
            topic.setLabel("Science");
            topicRepository.save(topic);
        }

        topicList = topicRepository.findByLabel("Business");
        if(topicList == null || topicList.size() == 0){
            Topic topic2 = new Topic();
            topic2.setLabel("Business");
            topicRepository.save(topic2);
        }

        topicList = topicRepository.findByLabel("Media");
        if(topicList == null || topicList.size() == 0){
            Topic topic3 = new Topic();
            topic3.setLabel("Media");
            topicRepository.save(topic3);
        }
    }
}
