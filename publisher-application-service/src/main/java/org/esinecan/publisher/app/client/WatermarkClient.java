package org.esinecan.publisher.app.client;

import feign.RequestLine;
import org.esinecan.publisher.publication.model.WatermarkTicket;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * Feign Client that calls the Watermark Service's related endpoint.
 * Created by eren.sinecan
 */
@FeignClient(value = "publisher-watermarker")
public interface WatermarkClient {
    @RequestLine("POST /watermark-ticket")
    Boolean watermarkTicket(WatermarkTicket ticket);
}
