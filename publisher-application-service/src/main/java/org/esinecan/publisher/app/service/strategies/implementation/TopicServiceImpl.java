package org.esinecan.publisher.app.service.strategies.implementation;

import org.esinecan.publisher.app.service.strategies.contract.TopicService;
import org.esinecan.publisher.publication.model.Topic;
import org.esinecan.publisher.publication.repository.TopicRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * A service to get the list of all topics.
 *
 * It is entirely decoupled from 'Book' entity on the back end for extensibility reasons.
 *
 * Created by eren.sinecan
 */
@Service
public class TopicServiceImpl implements TopicService{
    @Autowired
    private TopicRepository topicRepository;

    private static final Logger logger = LoggerFactory.getLogger(TopicServiceImpl.class);

    /**
     * This endpoint returns the list of all topics so that our users can choose one when entering a new book.
     *
     * @return list of all topics
     */
    @Override
    public List<Topic> getAllTopics() {
        List<Topic> topics = topicRepository.findAll();

        logger.debug("Topic list retrieved:{}", topics);

        return topics;
    }
}
