package org.esinecan.publisher.app.service.strategies.implementation;

import org.esinecan.publisher.app.service.strategies.contract.AppUserService;
import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.gateway.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by eren.sinecan
 */
@Service
public class AppUserServiceImpl implements AppUserService {

    @Autowired
    private UserRepository userRepository;

    /**
     * A handy service method for the occasional validation
     *
     * @param username username acquired from the Principal at controller level.
     * @return Our custom User object that is also an entity.
     */
    public User getUserByUsername(String username){
        return userRepository.findByUsername(username);
    }
}
