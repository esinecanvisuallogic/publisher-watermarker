package org.esinecan.publisher.app.web.validation.errorhandler;

import org.esinecan.publisher.exception.BaseExceptionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Custom ControllerAdvice for app-service endpoints.
 *
 * Created by eren.sinecan
 */
@RestControllerAdvice
public class AppServiceExceptionHandler extends BaseExceptionHandler {
    public AppServiceExceptionHandler(){
        super();
        registerMapping(IllegalStateException.class, "WRONG_ENUM_CONFIGURATION", "Please keep insurance module values unique", HttpStatus.SERVICE_UNAVAILABLE);
        registerMapping(IllegalArgumentException.class, "ILLEGAL_REQUEST", "Dto has some unusual values. It was probably tampered with", HttpStatus.BAD_REQUEST);
        registerMapping(BadCredentialsException.class, "USER_NOT_CORRECT", "Request user does not match request principal", HttpStatus.CONFLICT);
    }
}