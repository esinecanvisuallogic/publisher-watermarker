package org.esinecan.publisher.app.configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Reads some initial parameters.
 * Created by eren.sinecan
 */
@Component
@Getter
public class AppInitializationPropertiesConfiguration {

    @Value("${authorizationConstants.privilege.read:READ_PRIVILEGE}")
    private String readPrivilege;

    @Value("${authorizationConstants.privilege.write:WRITE_PRIVILEGE}")
    private String writePrivilege;

    @Value("${authorizationConstants.role.user.name:USER}")
    private String userRole;

    @Value("${authorizationConstants.role.user.afterLoginUrl:/ui/}")
    private String afterLoginUrl;
}
