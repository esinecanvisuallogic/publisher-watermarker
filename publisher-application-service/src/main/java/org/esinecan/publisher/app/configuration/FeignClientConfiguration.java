package org.esinecan.publisher.app.configuration;

import feign.Contract;
import feign.RequestInterceptor;
import feign.auth.BasicAuthRequestInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by eren.sinecan
 */
@Configuration
public class FeignClientConfiguration {

    @Value("${feignconf.user:admin}")
    private String user;

    @Value("${feignconf.pass:adminpass}")
    private String pass;

    /**
     * We configure Feign to use default Feign contract instead of SpringMVCContract
     *
     * @return Default Feign contract
     */
    @Bean
    public Contract feignContract() {
        return new feign.Contract.Default();
    }

    /**
     * We configure feign to have basic auth headers.
     *
     * @return BasicAuthRequestInterceptor
     */
    @Bean
    public RequestInterceptor requestInterceptor() {
        return new BasicAuthRequestInterceptor(user, pass);
    }
}
