Feature: Book registration

Narrative:
As a user
I want to be able to register books
So that I can test watermark feature

Scenario: Register a new book

Given I have a registered 'User' called 'bookagent@publisher.com'
  And I have a 'BookDTO' with 'type', 'title', 'author', 'topic'
When I call 'publicationRegistration' service with 'BookDTO'
  And I call 'retrievePublication' service
Then A non-empty 'Book' object should return
  And 'Book' should have the same 'title' as 'BookDto'
  And 'Book' should have the same 'type' as 'BookDto'
  And 'Book' should have the same 'author' as 'BookDto'
  And 'Book' should have the same 'topic' as 'BookDto'
  And 'Book' should have empty 'WatermarkTicket'