Feature: Querying Publications

Narrative:
As a user
I want to be able to query my publications
Based on watermark status

Scenario: Query bunch of publications of varying statuses

Given I have a 'Journal' with title 'J1' and 'watermarkTicket' as 'null'
  And I have a 'Book' with title 'B1' and 'watermarkTicket' as 'false'
  And I have a 'Journal' with title 'J2' and 'watermarkTicket' as 'true'
When I call 'getMyPublication' service with no exclusion
Then All three of my publications should return
When I call 'getMyPublication' service with 'excludeWatermarked'
Then 'J1' and 'B1' should return
When I call 'getMyPublication' service with 'excludeWatermarked' and 'excludeWatermarkInProgress'
Then Only 'J1' should return
When I call 'getMyPublication' service with all exclusions
Then An empty list should return

