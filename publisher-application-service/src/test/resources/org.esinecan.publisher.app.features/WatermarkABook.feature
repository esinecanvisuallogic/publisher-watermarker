Feature: Book watermark

Narrative:
As a user
I want to be able to watermark books
Asynchronously

Scenario: Watermark a new book

Given I have a 'Book' with 'watermarkTicket' as 'null'
When I call 'watermark' service with 'Book'
Then A non-empty 'WatermarkTicket' object should return
  And 'Book' should have 'watermarkTicket' as 'false'
