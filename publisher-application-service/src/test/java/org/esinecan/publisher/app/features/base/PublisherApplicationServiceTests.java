package org.esinecan.publisher.app.features.base;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.embedded.RedisServer;

import java.io.IOException;

public abstract class PublisherApplicationServiceTests {

    private static RedisServer redisServer;

    private static final Logger logger = LoggerFactory.getLogger(PublisherApplicationServiceTests.class);
    
    @BeforeClass
    public static void before() throws IOException {
        try {
            RedisServer redisServerlet = new RedisServer(6379);
            redisServerlet.start();
            redisServer = redisServerlet;
        }catch (RuntimeException e){
            logger.error("Failed to start Redis Server. " +
                    "If there isn't an instance already running, application will have errors");
        }
    }

    @AfterClass
    public static void after() throws IOException {
        try {
            redisServer.stop();
        }catch (NullPointerException e){
            logger.error("Redis is not running");
        }
    }
}