package org.esinecan.publisher.app.features;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.esinecan.publisher.app.PublisherApplicationServiceApplication;
import org.esinecan.publisher.app.service.PublicationStrategyWrapperService;
import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.gateway.repository.UserRepository;
import org.esinecan.publisher.publication.model.Book;
import org.esinecan.publisher.publication.model.Topic;
import org.esinecan.publisher.publication.model.WatermarkTicket;
import org.esinecan.publisher.publication.repository.BookRepository;
import org.esinecan.publisher.publication.repository.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import static java.util.Objects.isNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by eren.sinecan
 */
@ContextConfiguration(
        loader = SpringBootContextLoader.class,
        classes = PublisherApplicationServiceApplication.class
)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WatermarkABookSteps {
    private User user;

    private Book bookDTO;

    private Book book;

    private WatermarkTicket watermarkTicket;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PublicationStrategyWrapperService publicationStrategyWrapperService;

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private BookRepository bookRepository;

    @Given("^I have a 'Book' with 'watermarkTicket' as 'null'$")
    public void i_have_a_Book_with_watermarkTicket_as_null() throws Throwable {
        user = new User();
        user.setUsername("bookagent@publisher.com");
        user.setPassword("password");
        userRepository.save(user);

        Topic topic = new Topic();
        topic.setLabel("Science");
        topicRepository.save(topic);

        Topic topic1 = new Topic();

        topic1.setId(topic.getId());
        topic1.setLabel(topic.getLabel());

        bookDTO = new Book();
        bookDTO.setTopic(topic1);
        bookDTO.setAuthor("Test Testington");
        bookDTO.setContent("Test Content");
        bookDTO.setTitle("Test Title");
        bookDTO.setUser(user);

        publicationStrategyWrapperService.registerNewPublication(user, bookDTO);
        book = (Book) publicationStrategyWrapperService.getMyPublications(user, false, false, false).get(0);
    }

    @When("^I call 'watermark' service with 'Book'$")
    public void i_call_watermark_service_with_Book() throws Throwable {
        watermarkTicket = publicationStrategyWrapperService.watermarkPublication(book.getWatermarkTicket());
        watermarkTicket = publicationStrategyWrapperService.isWatermarkComplete(watermarkTicket.getId());

        book = bookRepository.findByWatermarkTicket(watermarkTicket);
    }

    @Then("^A non-empty 'WatermarkTicket' object should return$")
    public void a_non_empty_Book_object_should_return() throws Throwable {
        assertTrue(!isNull(watermarkTicket));
    }

    @Then("^'Book' should have 'watermarkTicket' as 'false'$")
    public void book_should_have_watermarkTicket_as_false() throws Throwable {
        assertEquals(book.getWatermarkTicket().getCompleted(), Boolean.FALSE);
    }
}
