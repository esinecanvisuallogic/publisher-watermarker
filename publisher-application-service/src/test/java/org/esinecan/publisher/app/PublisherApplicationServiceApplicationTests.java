package org.esinecan.publisher.app;

import org.esinecan.publisher.app.features.base.PublisherApplicationServiceTests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration-test")
public class PublisherApplicationServiceApplicationTests extends PublisherApplicationServiceTests {

	@Test
	public void contextLoads() {
	}
}
