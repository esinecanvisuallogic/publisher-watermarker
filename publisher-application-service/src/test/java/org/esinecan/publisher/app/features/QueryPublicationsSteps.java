package org.esinecan.publisher.app.features;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.esinecan.publisher.app.PublisherApplicationServiceApplication;
import org.esinecan.publisher.app.service.PublicationStrategyWrapperService;
import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.gateway.repository.UserRepository;
import org.esinecan.publisher.publication.model.Book;
import org.esinecan.publisher.publication.model.Journal;
import org.esinecan.publisher.publication.model.Topic;
import org.esinecan.publisher.publication.model.WatermarkTicket;
import org.esinecan.publisher.publication.model.base.IBasePublication;
import org.esinecan.publisher.publication.repository.BookRepository;
import org.esinecan.publisher.publication.repository.JournalRepository;
import org.esinecan.publisher.publication.repository.TopicRepository;
import org.esinecan.publisher.publication.repository.WatermarkTicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan
 */
@ContextConfiguration(
        loader = SpringBootContextLoader.class,
        classes = PublisherApplicationServiceApplication.class
)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class QueryPublicationsSteps {
    private User user;

    private Journal J1;

    private Journal J2;

    private Book B1;

    private Topic topic;

    private List<IBasePublication> noExclusion;

    private List<IBasePublication> excludeWatermarked;

    private List<IBasePublication> excludeWatermarkedAndExcludeWatermarkInProgress;

    private List<IBasePublication> allThree;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PublicationStrategyWrapperService publicationStrategyWrapperService;

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private WatermarkTicketRepository watermarkTicketRepository;

    @Autowired
    private JournalRepository journalRepository;

    @Autowired
    private BookRepository bookRepository;

    @Given("^I have a 'Journal' with title 'J(\\d+)' and 'watermarkTicket' as 'null'$")
    public void i_have_a_Journal_with_title_J_and_watermarkTicket_as_null(int arg1) throws Throwable {
        init();

        WatermarkTicket ticket = new WatermarkTicket();
        ticket.setCompleted(false);
        ticket.setStarted(false);
        watermarkTicketRepository.saveAndFlush(ticket);

        J1 = new Journal();
        J1.setWatermarkTicket(ticket);
        J1.setAuthor("Test Testington");
        J1.setContent("Test Content");
        J1.setTitle("J1");
        J1.setUser(user);

        journalRepository.saveAndFlush(J1);
    }

    @Given("^I have a 'Book' with title 'B(\\d+)' and 'watermarkTicket' as 'false'$")
    public void i_have_a_Book_with_title_B_and_watermarkTicket_as_false(int arg1) throws Throwable {
        WatermarkTicket ticket = new WatermarkTicket();
        ticket.setCompleted(false);
        ticket.setStarted(true);
        watermarkTicketRepository.saveAndFlush(ticket);

        B1 = new Book();
        B1.setTopic(topic);
        B1.setAuthor("Test Testington");
        B1.setContent("Test Content");
        B1.setTitle("B1");
        B1.setUser(user);
        B1.setWatermarkTicket(ticket);

        bookRepository.saveAndFlush(B1);
    }

    @Given("^I have a 'Journal' with title 'J(\\d+)' and 'watermarkTicket' as 'true'$")
    public void i_have_a_Journal_with_title_J_and_watermarkTicket_as_true(int arg1) throws Throwable {
        WatermarkTicket ticket = new WatermarkTicket();
        ticket.setCompleted(true);
        ticket.setStarted(true);
        watermarkTicketRepository.saveAndFlush(ticket);

        J2 = new Journal();
        J2.setWatermarkTicket(ticket);
        J2.setAuthor("Test Testington");
        J2.setContent("Test Content");
        J2.setTitle("J2");
        J2.setUser(user);

        journalRepository.saveAndFlush(J2);
    }

    @When("^I call 'getMyPublication' service with no exclusion$")
    public void i_call_getMyPublication_service_with_no_exclusion() throws Throwable {
        noExclusion = publicationStrategyWrapperService.getMyPublications(user, false, false, false);
    }

    @Then("^All three of my publications should return$")
    public void all_three_of_my_publications_should_return() throws Throwable {
        assertEquals(noExclusion.size(), 3);
    }

    @When("^I call 'getMyPublication' service with 'excludeWatermarked'$")
    public void i_call_getMyPublication_service_with_excludeWatermarked() throws Throwable {
        excludeWatermarked = publicationStrategyWrapperService.getMyPublications(user, true, false, false);
    }

    @Then("^'J(\\d+)' and 'B(\\d+)' should return$")
    public void j_and_B_should_return(int arg1, int arg2) throws Throwable {
        assertEquals(excludeWatermarked.size(), 2);

        assertEquals(((Book) excludeWatermarked.get(0)).getTitle(), "B1");

        assertEquals(((Journal) excludeWatermarked.get(1)).getTitle(), "J1");
    }

    @When("^I call 'getMyPublication' service with 'excludeWatermarked' and 'excludeWatermarkInProgress'$")
    public void i_call_getMyPublication_service_with_excludeWatermarked_and_excludeWatermarkInProgress() throws Throwable {
        excludeWatermarkedAndExcludeWatermarkInProgress =
                publicationStrategyWrapperService.getMyPublications(user, true, true, false);
    }

    @Then("^Only 'J(\\d+)' should return$")
    public void only_J_should_return(int arg1) throws Throwable {
        assertEquals(excludeWatermarkedAndExcludeWatermarkInProgress.size(), 1);

        assertEquals(((Journal) excludeWatermarkedAndExcludeWatermarkInProgress.get(0)).getTitle(), "J1");
    }

    @When("^I call 'getMyPublication' service with all exclusions$")
    public void i_call_getMyPublication_service_with_all_exclusions() throws Throwable {
        allThree = publicationStrategyWrapperService.getMyPublications(user, true, true, true);
    }

    @Then("^An empty list should return$")
    public void an_empty_list_should_return() throws Throwable {
        assertEquals(allThree.size(), 0);
    }

    private void init(){
        user = new User();
        user.setUsername("queryagent@publisher.com");
        user.setPassword("password");
        userRepository.saveAndFlush(user);

        topic = new Topic();
        topic.setLabel("Science");
        topicRepository.saveAndFlush(topic);
    }
}
