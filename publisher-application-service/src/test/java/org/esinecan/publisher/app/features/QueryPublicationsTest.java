package org.esinecan.publisher.app.features;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by eren.sinecan
 */
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/org.esinecan.publisher.app.features/QueryPublications.feature")
public class QueryPublicationsTest {
}
