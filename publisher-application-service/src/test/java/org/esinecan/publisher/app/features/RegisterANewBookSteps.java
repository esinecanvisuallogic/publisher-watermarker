package org.esinecan.publisher.app.features;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.esinecan.publisher.app.PublisherApplicationServiceApplication;
import org.esinecan.publisher.app.service.PublicationStrategyWrapperService;
import org.esinecan.publisher.gateway.model.User;
import org.esinecan.publisher.gateway.repository.UserRepository;
import org.esinecan.publisher.publication.model.Book;
import org.esinecan.publisher.publication.model.Topic;
import org.esinecan.publisher.publication.repository.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import static java.util.Objects.isNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by eren.sinecan
 */
@ContextConfiguration(
        loader = SpringBootContextLoader.class,
        classes = PublisherApplicationServiceApplication.class
)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RegisterANewBookSteps {
    private User user;

    private Book bookDTO;

    private Book book;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PublicationStrategyWrapperService publicationStrategyWrapperService;

    @Autowired
    private TopicRepository topicRepository;

    @Given("^I have a registered 'User' called 'bookagent@publisher\\.com'$")
    public void i_have_a_registered_User_called_bookagent_publisher_com() throws Throwable {
        user = new User();
        user.setUsername("bookagent@publisher.com");
        user.setPassword("password");
        userRepository.save(user);
    }

    @Given("^I have a 'BookDTO' with 'type', 'title', 'author', 'topic'$")
    public void i_have_a_BookDTO_with_type_title_author_topic() throws Throwable {
        Topic topic = new Topic();
        topic.setLabel("Science");
        topicRepository.save(topic);

        Topic topic1 = new Topic();

        topic1.setId(topic.getId());
        topic1.setLabel(topic.getLabel());

        bookDTO = new Book();
        bookDTO.setTopic(topic1);
        bookDTO.setAuthor("Test Testington");
        bookDTO.setContent("Test Content");
        bookDTO.setTitle("Test Title");
        bookDTO.setUser(user);
        bookDTO.setWatermarkTicket(null);
    }

    @When("^I call 'publicationRegistration' service with 'BookDTO'$")
    public void i_call_publicationRegistration_service_with_BookDTO() throws Throwable {
        publicationStrategyWrapperService.registerNewPublication(user, bookDTO);
    }

    @When("^I call 'retrievePublication' service$")
    public void i_call_retrievePublication_service() throws Throwable {
        book = (Book) publicationStrategyWrapperService.getMyPublications(user, false, false, false).get(0);
    }

    @Then("^A non-empty 'Book' object should return$")
    public void a_non_empty_Book_object_should_return() throws Throwable {
        assertTrue(!isNull(book));
    }

    @Then("^'Book' should have the same 'title' as 'BookDto'$")
    public void book_should_have_the_same_title_as_BookDto() throws Throwable {
        assertEquals(book.getTitle(), bookDTO.getTitle());
    }

    @Then("^'Book' should have the same 'type' as 'BookDto'$")
    public void book_should_have_the_same_type_as_BookDto() throws Throwable {
        assertTrue(book instanceof Book);
    }

    @Then("^'Book' should have the same 'author' as 'BookDto'$")
    public void book_should_have_the_same_author_as_BookDto() throws Throwable {
        assertEquals(book.getAuthor(), bookDTO.getAuthor());
    }

    @Then("^'Book' should have the same 'topic' as 'BookDto'$")
    public void book_should_have_the_same_topic_as_BookDto() throws Throwable {
        assertEquals(book.getTopic().getId(), bookDTO.getTopic().getId());
    }

    @Then("^'Book' should have empty 'WatermarkTicket'$")
    public void book_should_have_empty_WatermarkTicket() throws Throwable {
        assertEquals(book.getWatermarkTicket().getCompleted(), false);
        assertEquals(book.getWatermarkTicket().getStarted(), false);
    }
}
